from django.urls import path, include

from backend import views

urlpatterns = [
    path('test', views.test, name='test'),
    path('<int:document_id>/', views.document, name='document'),
    path('', views.documents, name='documents'),
    path('save/', views.save, name='save'),
    path('login/', views.log_in, name='login')
]
