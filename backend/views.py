import json
import base64

from django.contrib.auth import authenticate, login
from django.core import serializers
from django.http import HttpResponse
from django.middleware.csrf import get_token
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from backend.models import Document


def test(request):
    return HttpResponse("Hello, this is a test response")


def document(request, document_id):
    return HttpResponse(get_object_or_404(Document, pk=document_id))


def documents(request):
    qs = Document.objects.all()
    qs_json = serializers.serialize('json', qs)
    return HttpResponse(qs_json, content_type='application/json')


def save(request):
    # todo add auth check
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        name = body['name']
        comment = body.get('comment')
        file = base64.b64encode(body['file'].encode('utf-8'))
        doc = Document(name=name,
                       comment=comment,
                       file=file
                       )
        doc.save()
        return HttpResponse(Document.objects.get(id=doc.id))


@csrf_exempt
def log_in(request):
    if request.method == 'POST':
        body = json.loads(request.body)
        username = body['username']
        password = body['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponse(status=200)
