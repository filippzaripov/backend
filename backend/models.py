import json

from django.db import models


class Document(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    comment = models.CharField(max_length=500, null=True)
    file = models.BinaryField(blank=False, null=False)

    def __str__(self):
        json_doc = json.dumps({
            "name": self.name,
            "comment": self.comment,
            "file": str(self.file)
        })
        return str(json_doc)
